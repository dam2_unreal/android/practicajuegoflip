package com.example.practicajuegoflip;

import java.util.Random;

public class Tablero {

    private int filas, columnas, elementos, tablero[][];

    public Tablero(int f, int c, int e){
        filas = f;
        columnas = c;
        elementos = e;
        tablero = new int[f][c];

        generarTablero();
    }

    private void generarTablero() {
        Random r = new Random();

        for (int i=0;i<filas;i++){
            for (int j=0;j<columnas;j++){
                tablero[i][j] = r.nextInt(elementos);
            }
        }
    }

    public int [][] getTablero(){
        return tablero;
    }
}
