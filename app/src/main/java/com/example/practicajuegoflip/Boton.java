package com.example.practicajuegoflip;

import android.content.Context;
import androidx.appcompat.widget.AppCompatButton;

public class Boton extends AppCompatButton {

    private int fila, columnas, numElementos, fondo, id, contenido;

    public Boton(Context context, int f, int c, int max, int fon, int id, int cont) {
        super(context);

        this.fila = f;
        this.columnas = c;
        this.numElementos = max;
        this.fondo = fon;
        this.id = id;
        this.contenido = cont;

        this.setBackgroundResource(fondo);
    }

    public int getFila() {
        return fila;
    }

    public int getColumnas() {
        return columnas;
    }

    @Override
    public int getId() {
        return id;
    }

    /**
     * Al pulsar el boton cambiaremos el boton de numero/color
     * @return El numero/color siguiente
     */
    public int getNuevoFondo(){
        contenido++;
        if(contenido == numElementos) contenido = 0;
        return contenido;
    }

}