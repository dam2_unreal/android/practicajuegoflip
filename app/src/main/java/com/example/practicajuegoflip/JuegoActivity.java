package com.example.practicajuegoflip;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;

public class JuegoActivity extends BaseActivity {

    public static final String NUMCLICK = "Numero de click";

    private LinearLayout lyJuego;
    private Chronometer crono;
    private TextView tvNumClick;

    private Vibrator vibrator;
    private MediaPlayer mp;

    private boolean vibrar, sonar, fichas;

    private int filas, columnas, elementos, clicks;
    private int [] panel;
    private int [][] miTablero, miTableroId;

    private Boton [][] botones;

    private static int [] colores = {
        R.drawable.ic_1c,
        R.drawable.ic_2c,
        R.drawable.ic_3c,
        R.drawable.ic_4c,
        R.drawable.ic_5c,
        R.drawable.ic_6c
    };

    private static int [] numeros = {
        R.drawable.ic_1n,
        R.drawable.ic_2n,
        R.drawable.ic_3n,
        R.drawable.ic_4n,
        R.drawable.ic_5n,
        R.drawable.ic_6n
    };

    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        cargarViews();
        pantallaCompleta();
        cargarOpciones();
        iniciaJuego();
    }

    //----------------------------------------------------------------------------------------------

    private void cargarViews() {
        lyJuego = (LinearLayout) findViewById(R.id.lyJuego);
        crono = (Chronometer) findViewById(R.id.crono);
        tvNumClick = (TextView) findViewById(R.id.tvPulsa);

        lyJuego.removeAllViews();
    }

    private void cargarOpciones() {
        Bundle datos = getIntent().getExtras();

        filas = datos.getInt(MainActivity.FILAS);
        columnas = datos.getInt(MainActivity.COLUMNAS);
        elementos = datos.getInt(MainActivity.ELEMENTOS);

        vibrar = datos.getBoolean(MainActivity.VIBRAR);
        sonar = datos.getBoolean(MainActivity.SONAR);
        fichas = datos.getBoolean(MainActivity.FICHAS);

        panel = fichas?numeros:colores; // Numeros o Colores
        miTablero = (new Tablero(filas, columnas, elementos)).getTablero();
        miTableroId = new int[filas][columnas];
        botones = new Boton[filas][columnas];
    }

    /**
     * iniciaJuego();
     *
     * -Inicializamos la vibracion y/o sonido
     * -Iniciamos el cronometro
     * -Configuramos el layour vacio hasta ahora
     */
    private void iniciaJuego() {
        clicks = 0;

        if(vibrar) vibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        if(sonar) mp = MediaPlayer.create(this, R.raw.touch);

        crono.start();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int alturaBoton = (dm.heightPixels)/filas;

        int idBoton = 0;

        /**
         * for
         *
         * 1-Creo un LinearLayout Horizontal para cada fila
         * 2-Creamos los botones
         * 3-Establezco los parametros de layout al boton
         * 4-Añadir listener a botones
         * 5-añado el boton al layout
         * 6-Añadir el layout a mi layout vertical
         */
        for(int f=0; f<filas; f++){
            LinearLayout lfila = new LinearLayout(this); //1
            lfila.setOrientation(LinearLayout.HORIZONTAL);       //1

            for(int c=0; c<columnas; c++){
                botones[f][c] = new Boton(this, f, c, elementos, panel[miTablero[f][c]], ++idBoton, miTablero[f][c]);   //2
                miTableroId[f][c] = idBoton;

                botones[f][c].setLayoutParams(new LinearLayout.LayoutParams(0, alturaBoton, 1.0f)); //3

                botones[f][c].setOnClickListener(new View.OnClickListener() { //4
                    @Override
                    public void onClick(View v) {
                        int miFila = ((Boton)v).getFila();
                        int miColumna = ((Boton)v).getColumnas();
                        pulsarBoton(miFila, miColumna);
                    }
                });

                lfila.addView(botones[f][c]); //5
            }
            lyJuego.addView(lfila); //6
        }
    }

    public boolean checkGanar(){
        int valor = miTablero[0][0];
        for (int i = 0;i<filas;i++){
            for(int j = 0;j<columnas;j++){
                if(miTablero[i][j] != valor) return false;
            }
        }
        return true;
    }

    public void terminarTodo() {
        Intent i = new Intent();
        i.putExtra(NUMCLICK, clicks);
        setResult(RESULT_OK, i);
        finish();
    }

    //----------------------------------------------------------------------------------------------

    public void pulsarBoton(int f, int c){
        if(sonar) mp.start();
        if(vibrar) vibrator.vibrate(100);
        clicks++;
        tvNumClick.setText(""+clicks);

        //Si he pulsado una esquina
        if((f==0 && c==0) || (f==0 && c==columnas-1) || (f==filas-1 && c==0) || (f==filas-1 && c==columnas-1)){
            cambiar1(f,c);
        }else{
            for(int i = maximo(f-1, 0);i<=minimo(f+1, filas-1);i++){
                for (int j = maximo(0,c-1);j<=minimo(c+1,columnas-1);j++){
                    if(f!=i && c!=j) continue;
                    cambiarFondo(i,j);
                }
            }
        }

        if(checkGanar()){
            terminarTodo();
        }
    }

    //----------------------------------------------------------------------------------------------

    private void cambiar1(int f, int c) {

        if(f==0 && c==0){
            cambiarFondo(0,0);
            cambiarFondo(0,1);
            cambiarFondo(1,0);
            cambiarFondo(1,1);
        }

        if(f==0 && c==columnas-1){
            cambiarFondo(0,c-1);
            cambiarFondo(0,c);
            cambiarFondo(1,c-1);
            cambiarFondo(1,c);
        }

        if(f==filas-1 && c==0){
            cambiarFondo(f-1,0);
            cambiarFondo(f-1,1);
            cambiarFondo(f,0);
            cambiarFondo(f,1);
        }

        if(f==filas-1 && c==columnas-1){
            cambiarFondo(f-1,c-1);
            cambiarFondo(f-1,c);
            cambiarFondo(f,c-1);
            cambiarFondo(f,c);
        }

    }

    //----------------------------------------------------------------------------------------------

    private void cambiarFondo(int f, int c){
        int fondo = botones[f][c].getNuevoFondo();
        botones[f][c].setBackgroundResource(panel[fondo]);
        miTablero[f][c] = fondo; //Actualiza el tablero
    }

    //----------------------------------------------------------------------------------------------

    public int maximo(int a, int b) {return (a>=b)?a:b;}
    public int minimo(int a, int b) {return (a<=b)?a:b;}
}