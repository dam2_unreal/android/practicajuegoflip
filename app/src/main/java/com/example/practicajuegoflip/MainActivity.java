package com.example.practicajuegoflip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class MainActivity extends BaseActivity implements SeekBar.OnSeekBarChangeListener{

    public static final String FILAS = "filas", COLUMNAS = "columnas", ELEMENTOS = "elementos";
    public static final String SONAR = "sonar", VIBRAR ="vibrar", FICHAS = "fichas";
    public static final int REQUEST_CODE = 1234;

    private TextView tvFila2,tvColumna2,tvElemento2;
    private SeekBar sbFila, sbColumna, sbElemento;
    private Switch swVibrar, swSonido;
    private RadioGroup rgJuego;
    private RadioButton rbtnNumeros, rbtnColores;
    private Button btnJuego;

    private int nFilas, nColumnas, nElementos;
    private boolean vibrar, sonar, isNumero;

    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cargarView();
        inicializarSeekBars();
        ponerProgreso();
        ponerListener();
        pantallaCompleta();
    }

    //----------------------------------------------------------------------------------------------

    private void cargarView() {
        tvFila2 = (TextView) findViewById(R.id.tvFila2);
        tvColumna2 = (TextView) findViewById(R.id.tvColumna2);
        tvElemento2 = (TextView) findViewById(R.id.tvElemento2);
        sbFila = (SeekBar) findViewById(R.id.sbFila);
        sbColumna = (SeekBar) findViewById(R.id.sbColumna);
        sbElemento = (SeekBar) findViewById(R.id.sbElemento);
        swVibrar = (Switch) findViewById(R.id.swVibrar);
        swSonido = (Switch) findViewById(R.id.swSonido);
        rgJuego = (RadioGroup) findViewById(R.id.rgJuego);
        rbtnNumeros = (RadioButton) findViewById(R.id.rbtnNumeros);
        rbtnColores = (RadioButton) findViewById(R.id.rbtnColores);
        btnJuego = (Button) findViewById(R.id.btnJugar);
    }

    private void inicializarSeekBars() {
        nFilas = nColumnas = 3;
        nElementos = 2;
    }

    private void ponerProgreso() {
        tvFila2.setText((String.format(getResources().getString(R.string.tvFila2), nFilas)));
        tvColumna2.setText((String.format(getResources().getString(R.string.tvColumna2), nColumnas)));
        tvElemento2.setText((String.format(getResources().getString(R.string.tvElemento2), nElementos)));
    }

    private void ponerListener() {
        sbFila.setOnSeekBarChangeListener(this);
        sbColumna.setOnSeekBarChangeListener(this);
        sbElemento.setOnSeekBarChangeListener(this);
    }

    //----------------------------------------------------------------------------------------------

    public void jugar(View v){
        Intent i = new Intent(this, JuegoActivity.class);
        Bundle datos = new Bundle();

        isNumero = rbtnNumeros.isChecked();
        vibrar = swVibrar.isChecked();
        sonar = swSonido.isChecked();

        datos.putInt(FILAS, nFilas);
        datos.putInt(COLUMNAS, nColumnas);
        datos.putInt(ELEMENTOS, nElementos);
        datos.putBoolean(VIBRAR, vibrar);
        datos.putBoolean(SONAR, sonar);
        datos.putBoolean(FICHAS, isNumero);

        i.putExtras(datos);
        startActivityForResult(i, REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE && resultCode==RESULT_OK){
            int clics=data.getIntExtra(JuegoActivity.NUMCLICK, -1);

            AlertDialog.Builder mensaje = new AlertDialog.Builder(this);
            String texto = String.format(getResources().getString(R.string.felicitaciones),clics);
            mensaje.setMessage(texto);
            mensaje.setPositiveButton("Aceptar",null);
            mensaje.show();
        }

        if(requestCode==REQUEST_CODE && requestCode==RESULT_CANCELED){
            Toast.makeText(this,getResources().getString(R.string.cancelar),Toast.LENGTH_SHORT).show();
        }

    }

    //----------------------------------------------------------------------------------------------

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        if(seekBar == sbFila) nFilas = progress+3;
        if(seekBar == sbColumna) nColumnas = progress+3;
        if(seekBar == sbElemento) nElementos = progress+2;
        ponerProgreso();

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    //----------------------------------------------------------------------------------------------

}